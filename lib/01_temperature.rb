def ftoc(temp)
  float_temp = (temp - 32) * (5.0/9)
  float_temp.ceil
end

def ctof(temp)
  (temp * (9/5.0)) + 32
end
