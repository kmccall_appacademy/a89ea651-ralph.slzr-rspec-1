def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string,num=2)
  str_array = []
  while num > 0
    str_array << string
    num -= 1
  end
  str_array.join(' ')
end

def start_of_word(string, num)
  string.slice(0,num)
end

def first_word(string)
  str_arr = string.split(' ')
  str_arr[0]
end

def titleize(string)
  str_arr = string.split(' ')
  litte_words = ['and', 'of', 'the', 'over']
  arr = []
  str_arr.each_with_index do |word,idx|
    if litte_words.include?(word) && idx > 0
      arr << word
    else
      arr << word.capitalize
    end
  end

  arr.join(' ')
end
