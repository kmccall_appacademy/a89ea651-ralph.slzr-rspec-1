def word_check(str)
  str.each_char.with_index do |ch,idx|
    if idx == 0 && str[1] == 'q'
      return 'second q'
    elsif str.include?('q')
      return 'start q'
    elsif idx == 0 && str[1] == 'h' && str[2] == 'r'
      return 'three consonants'
    elsif idx == 0 && str[1] == 'c' && str[2] == 'h'
        return 'three consonants'
    elsif idx == 0 && str[1] == 'h'
      return 'two consonants'
    elsif idx == 0 && str[1] == 'r'
      return 'two consonants'
    elsif 'aeiou'.include?(ch)
      return 'vowel'
    elsif !'aeiou'.include?(ch)
      return 'consonant'
    end
  end
end

def translate(string)
  str_arr = string.split(' ')
  translated = []
  str_arr.each do |word|
    if word_check(word) == 'vowel'
      translated << word + 'ay'
    elsif word_check(word) == 'consonant'
      translated << word.slice(1..-1) + word.slice(0) + 'ay'
    elsif word_check(word) == 'two consonants'
      translated << word.slice(2..-1) + word.slice(0..1) + 'ay'
    elsif word_check(word) == 'three consonants'
      translated << word.slice(3..-1) + word.slice(0..2) + 'ay'
    elsif word_check(word) == 'start q'
      translated << word.slice(2..-1) + word.slice(0..1) + 'ay'
    elsif word_check(word) == 'second q'
      translated << word.slice(3..-1) + word.slice(0..2) + 'ay'
    end
  end
  translated.join(' ')
end
